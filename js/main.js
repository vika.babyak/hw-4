// task 1
show("<b>Задание 1.</b> Напишите функцию map(fn, array), которая принимает на вход функцию и массив и обрабатывает каждый элемент массива этой функцией, возвращая новый массив.<br>");

show("<b>Результат:</b>");

// array
const arr = [1, 2, 3, 4, 5]
show(`Начальный массив: <b>${arr}</b>`);

// element processing
function processing(element) {
    return element *= element;
}

// map function
function map(fn, array) {
    for (let i = 0; i < array.length; i++) {
        array[i] = +fn(array[i]);
    }
    return array;
}
// show function
function show(showElement) {
    document.write(showElement + "<br>");
}

show(`Массив после обработки: <b>${map(processing, arr)}</b>`);

// task 2
show("<br><b>Задание 2.</b> Перепишите функцию, используя оператор '?' или '||'. Следующая функция возвращает true, если параметр age больше 18. В ином случае она задаёт вопрос confirm и возвращает его результат.");

show("<pre>function checkAge(age) {<br>    if (age > 18) {<br>       return true;<br>    } else {<br>         return confirm('Родители разрешили?');<br>      }<br>}</pre>");

// ask age
const age = prompt("Your age: ", 22);

// age check
const answer = age > 18 ? true : confirm('Родители разрешили?');
show(`<b>Результат: ${answer}</b>`);